USE [EasyQ-2013]
GO

/****** Object:  StoredProcedure [dbo].[ServeToken]    Script Date: 05/29/2016 11:36:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- Batch submitted through debugger: SQLQuery2.sql|7|0|C:\Users\ASUS\AppData\Local\Temp\~vsC178.sql
-- Batch submitted through debugger: SQLQuery8.sql|7|0|C:\Users\ASUS\AppData\Local\Temp\~vs40CA.sql
-- Batch submitted through debugger: SQLQuery4.sql|7|0|C:\Users\ASUS\AppData\Local\Temp\~vs9AF0.sql
-- =============================================
-- Author:		abi
-- Create date: 2013-08-29
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[ServeToken] 
	 --Add the parameters for the stored procedure here
	@appName varchar(100),
	@webfile varchar(100),
	@passport varchar(100),
	@serve_by varchar(100),
	@token int,
	@counter int,
	@procfee int, 
	@spfee int,
	@Visa_type varchar(100)
--	@Service int,
	
			
AS
BEGIN
DECLARE @MyDateTime DATETIME
SET @MyDateTime = DATEADD(ms,(30 * @counter)+200 ,GETDATE())
WAITFOR TIME @MyDateTime
DECLARE @Result int;
DECLARE @delay int = (30 * @counter)+200;
EXEC @Result = sp_getapplock @Resource = 'ServeToken', @LockMode = 'Exclusive', @LockTimeout = @delay --Time to wait for the lock

IF @Result <> 0
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;

Declare @sticker  int
Declare @date datetime 
DECLARE @dIncr DATE
DECLARE @fdate datetime
DECLARE @cn int = 1
DECLARE @cnt int = 30
DECLARE @count int 
DECLARE @lead int
--DECLARE @delay int = 2
--DECLARE @MyDateTime DATETIME
--SET @MyDateTime = DATEADD(ms,500 * @counter ,GETDATE())
--WAITFOR TIME @MyDateTime
--select @MyDateTime
	--Update tbl_service_log set ststop = GETDATE(), service =(SELECT DATEDIFF(SECOND, ststart,GETDATE())), flg=3 where tokenno = @token AND CAST(tissuetime AS DATE) = CAST(GETDATE() AS DATE)
	--delete from wait_token_queue where token_number = @token AND CAST(token_date AS DATE)= CAST(GETDATE() AS DATE)

select @sticker=(select COALESCE(MAX(sticker),0) AS sticker from tbl_AppointmentServed where CAST(Service_Date AS DATE) = CAST(GETDATE() AS DATE))		  

--select @lead = (select delivery_lead from tbl_centerInfo where sl=1)
--select @date = (select DATEADD(day,@lead,GETDATE()))
select @lead = (select delivery_lead from tbl_centerInfo where sl=1)
select @date = (select DATEADD(day,@lead,GETDATE()))
select @count =(select COUNT(*) from tbl_holiday where CAST(date AS DATE) > CAST(GETDATE() AS DATE) AND  CAST(date AS DATE) <= CAST(@date AS DATE))
select @date = (select DATEADD(day,(@lead+@count),GETDATE()))

IF NOT EXISTS 
    (select * from tbl_holiday where CAST(date AS DATE)=CAST(@date AS DATE))
    BEGIN
    select @fdate= @date
    END
ELSE
	BEGIN
	select @dIncr =(select DATEADD(day,1,@date))
	
	WHILE(@cn<@cnt)
		BEGIN
				IF NOT EXISTS 
					(select * from tbl_holiday where CAST(date AS DATE)=@dIncr)
					BEGIN
						 select @fdate=@dIncr
						 select @cn=@cnt
					END
				ELSE
					BEGIN
						SELECT @dIncr = DATEADD(DAY, 1, @dIncr )
						select @fdate =@dIncr
					END	
		select @cn=@cn+1			
		END
	END
select @count =(select COUNT(*) from tbl_holiday where CAST(date AS DATE) > CAST(GETDATE() AS DATE) AND  CAST(date AS DATE) <= CAST(@fdate AS DATE))
select @date = (select DATEADD(day,(@lead+@count),GETDATE()))
IF NOT EXISTS 
    (select * from tbl_holiday where CAST(date AS DATE)=CAST(@date AS DATE))
    BEGIN
    select @fdate= @date
    END
ELSE
	BEGIN
	select @dIncr =(select DATEADD(day,1,@date))
	
	WHILE(@cn<@cnt)
		BEGIN
				IF NOT EXISTS 
					(select * from tbl_holiday where CAST(date AS DATE)=@dIncr)
					BEGIN
						 select @fdate=@dIncr
						 select @cn=@cnt
					END
				ELSE
					BEGIN
						SELECT @dIncr = DATEADD(DAY, 1, @dIncr )
						select @fdate =@dIncr
					END	
		select @cn=@cn+1			
		END
	END
	select @count =(select COUNT(*) from tbl_holiday where CAST(date AS DATE) > CAST(GETDATE() AS DATE) AND  CAST(date AS DATE) <= CAST(@fdate AS DATE))
select @date = (select DATEADD(day,(@lead+@count),GETDATE()))
	insert into tbl_AppointmentServed
	(Applicant_name, WebFile_no, Passport, Service_Date, Delivery_Date, service_by, counter, token, sticker, proc_fee, sp_fee,Visa_type)
	VALUES(@appName, @webfile, @passport, GETDATE(),@fdate, @serve_by,@counter,@token, 
		  ((select COALESCE(MAX(sticker),0) AS Token from tbl_AppointmentServed where CAST(Service_Date AS DATE) = CAST(GETDATE() AS DATE)) + 1 ),
		  @procfee, @spfee,@Visa_type)
		  
	UPDATE tbl_AppointmentList set Presence_Status='ACCEPTED' where WebFile_no=@webfile AND Passport=@passport


--select DATEADD(day,(select delivery_lead from tbl_centerInfo where sl=1),GETDATE())
select (select COALESCE(MAX(sticker),0) AS Token from tbl_AppointmentServed where CAST(Service_Date AS DATE) = CAST(GETDATE() AS DATE)) as sticker, CAST(@fdate AS DATE) AS DELIVERY 
END
END

GO

